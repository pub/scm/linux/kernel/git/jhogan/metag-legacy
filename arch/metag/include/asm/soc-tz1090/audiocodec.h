#ifndef __SOC_TZ1090_AUDIOCODEC_H__
#define __SOC_TZ1090_AUDIOCODEC_H__

#define AUDIOCODEC_NUM_STEREOPAIRS 3

/* CR_AUDIO_ADC_CTRL fields */
#define AUDIO_PSCNTADC_L				(1 << 0)
#define AUDIO_PSCNTADC_R				(1 << 1)
#define AUDIO_GAINCTRL_MIC_P(x)			((x & 0x7) << 8)
#define AUDIO_GAINCTRL_MIC_P_MASK		AUDIO_GAINCTRL_MIC_P(0x7)
#define AUDIO_GAINCTRL_MIC_N(x)			((x & 0x7) << 12)
#define AUDIO_GAINCTRL_MIC_N_MASK		AUDIO_GAINCTRL_MIC_N(0x7)
#define AUDIO_GAINCTRL_LINE_L(x)		((x & 0x7) << 16)
#define AUDIO_GAINCTRL_LINE_L_MASK		AUDIO_GAINCTRL_LINE_L(0x7)
#define AUDIO_GAINCTRL_LINE_R(x)		((x & 0x7) << 20)
#define AUDIO_GAINCTRL_LINE_R_MASK		AUDIO_GAINCTRL_LINE_R(0x7)
#define AUDIO_GAINCTRL_ADC_L(x)			((x & 0x3) << 24)
#define AUDIO_GAINCTRL_ADC_L_MASK		AUDIO_GAINCTRL_ADC_L(0x3)
#define AUDIO_GAINCTRL_ADC_R(x)			((x & 0x3) << 28)
#define AUDIO_GAINCTRL_ADC_R_MASK		AUDIO_GAINCTRL_ADC_R(0x3)

/* CR_AUDIO_HP_CTRL fields */
#define AUDIO_PSCNTHP_L			(1 << 0)
#define AUDIO_PSCNTHP_R			(1 << 1)
#define AUDIO_RSTB_DIG_IP		(1 << 5)
#define AUDIO_RSTB_ANA_IP		(1 << 6)
#define AUDIO_RSTB_DIG_OP		(1 << 7)
#define AUDIO_RSTB_ANA_OP		(1 << 8)
#define AUDIO_PSCNT_PWM_A		(1 << 9)
#define AUDIO_PSCNT_PWM_B		(1 << 10)
#define AUDIO_PSCNT_PWM_C		(1 << 11)
#define AUDIO_PSCNT_PWM_D		(1 << 12)
#define AUDIO_PSCNT_PWM_E		(1 << 13)
#define AUDIO_PSCNT_PWM_F		(1 << 14)
#define AUDIO_PGA_MODE_SHIFT            16
#define AUDIO_PGA_MODE(x)		((x & 0x7) << AUDIO_PGA_MODE_SHIFT)
#define AUDIO_PGA_MODE_MASK		AUDIO_PGA_MODE(0x7)
#define AUDIO_RST_BG_IP			(1 << 20)
#define AUDIO_PWDN_BG_IP		(1 << 21)
#define AUDIO_RST_BG_OP			(1 << 22)
#define AUDIO_PWDN_BG_OP		(1 << 23)
#define AUDIO_I2S_EXT			(1 << 24)
#define AUDIO_PWDN_PLL			(1 << 28)

#define AUDIO_OUT_CONTROL_MAIN          (AUDIO_OUT_BASE_ADDR + 0x04)
#define AUDIO_OUT_SOFT_RESET            (AUDIO_OUT_BASE_ADDR + 0x08)

#define AUDIO_OUT_CM_ACTIVE_CHAN_SHIFT  13
#define AUDIO_OUT_CM_USB_MODE           (1 << 12)
#define AUDIO_OUT_CM_FRAME_SHIFT        7
#define AUDIO_OUT_CM_FRAME_MASK         0x3
#define AUDIO_OUT_CM_MASTER             (1 << 6)
#define AUDIO_OUT_CM_ACLK_SHIFT         5
#define AUDIO_OUT_CM_ACLK_MASK          0x1
#define AUDIO_OUT_CM_BLRCLK_EN          (1 << 4)
#define AUDIO_OUT_CM_LEFTPOL_0L1R       (0 << 3)
#define AUDIO_OUT_CM_LEFTPOL_1L0R       (1 << 3)
#define AUDIO_OUT_CM_BCLKPOL_RISING     (0 << 2)
#define AUDIO_OUT_CM_BCLKPOL_FALLING    (1 << 2)
#define AUDIO_OUT_CM_PACKED             (1 << 1)
#define AUDIO_OUT_CM_ME                 (1 << 0)

#define AUDIO_OUT_CC_FORMAT_SHIFT       4
#define AUDIO_OUT_CC_FORMAT_MASK        (0xf << AUDIO_OUT_CC_FORMAT_SHIFT)
#define AUDIO_OUT_CC_LEFT_JUST          (1 << 3)

enum ac_lrchannel {
	AUDIOCODEC_LEFT = 1,
	AUDIOCODEC_RIGHT = 2,
};

enum ac_mute {
	AUDIOCODEC_MUTE_NONE = 0x0,
	AUDIOCODEC_MUTE_HARD = 0x1,
	AUDIOCODEC_MUTE_90DB = 0x2,
	AUDIOCODEC_MUTE_SQUAREWAVE = 0x4,
};

enum ac_samplewidth {
	AUDIOCODEC_SAMPLEWIDTH_INVALID = -1,
	AUDIOCODEC_SAMPLEWIDTH_16 = 3,
	AUDIOCODEC_SAMPLEWIDTH_20 = 0,
	AUDIOCODEC_SAMPLEWIDTH_24 = 1,
	AUDIOCODEC_SAMPLEWIDTH_32 = 2,
};

enum ac_input {
	AUDIOCODEC_INPUT_INVALID = -1,
	AUDIOCODEC_INPUT_MIC = 0,
	AUDIOCODEC_INPUT_LINE,
	AUDIOCODEC_INPUT_IPOD,
	AUDIOCODEC_INPUT_MIC_DIFFERENTIAL,
};

enum ac_frame {
	AUDIOCODEC_FRAME_1616 = 0,
	AUDIOCODEC_FRAME_3232 = 2,
};

enum ac_clock {
	AUDIOCODEC_CLOCK_256FS = 0,
	AUDIOCODEC_CLOCK_384FS = 1,
};

enum ac_i2sclock {
	AUDIOCODEC_I2SCLOCK_INVALID = -1,
	AUDIOCODEC_I2SCLOCK_DISABLED = 0,
	AUDIOCODEC_I2SCLOCK_XTAL1,
	AUDIOCODEC_I2SCLOCK_XTAL2,
	AUDIOCODEC_I2SCLOCK_SYS_UNDELETED,
	AUDIOCODEC_I2SCLOCK_ADC_PLL,
};

enum ac_preset {
	AUDIOCODEC_PRESET_NONE = 0x0,
	AUDIOCODEC_PRESET_PHONO = 0x1,
#ifdef CONFIG_TZ1090_01XX_HDMI_AUDIO
	AUDIOCODEC_PRESET_HDMI = 0x2,
#endif
};

#endif /* __SOC_TZ1090_AUDIOCODEC_H__ */

