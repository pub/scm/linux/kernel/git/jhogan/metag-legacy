#ifndef _ASM_METAG_KDEBUG_H
#define _ASM_METAG_KDEBUG_H


enum die_val {
	DIE_UNUSED,
	DIE_OOPS = 1,
	DIE_TRAP,
};

#endif /* _ASM_METAG_KDEBUG_H */
