#ifndef _METAG_ASM_CORE_SYSFS_H
#define _METAG_ASM_CORE_SYSFS_H

#include <linux/device.h>

extern struct bus_type performance_subsys;
extern struct bus_type cache_subsys;

#endif /* _METAG_ASM_CORE_SYSFS_H */
