/*
 * PDP Desktop Graphics Framebuffer
 *
 * Copyright (c) 2008 Imagination Technologies Ltd.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */

#ifndef _PDPFB_GFX_H
#define _PDPFB_GFX_H

#include "pdpfb.h"

struct pdpfb_stream *pdpfb_gfx_get_stream(void);

#endif
